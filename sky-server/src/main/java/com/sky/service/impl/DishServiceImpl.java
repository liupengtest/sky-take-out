package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Employee;
import com.sky.entity.Setmeal;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlovorsMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {

    @Autowired
    DishMapper dishMapper;
    @Autowired
    DishFlovorsMapper dishFlovorsMapper;

    @Autowired
    SetmealDishMapper setmealDishMapper;

    @Autowired
    SetmealMapper setmealMapper;

    @Transactional
    @Override
    public void saveWithDishFlovors(DishDTO dishDTO) {
        //目标：插入一条菜品数据，插入多条口味数据
        //1.插入一条菜品数据
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.insert(dish);
        //获取菜品id
        Long dishId = dish.getId();

        //2.插入多条口味数据
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if(flavors!=null && flavors.size()>0){
            //给所有的口味添加菜品id
            flavors.stream().forEach(dishFlovor->{
                dishFlovor.setDishId(dishId);
            });

            dishFlovorsMapper.insertBatch(flavors);
        }
    }

    @Override
    public PageResult page(DishPageQueryDTO dishPageQueryDTO) {

        //1.设置分页参数
        PageHelper.startPage(dishPageQueryDTO.getPage(), dishPageQueryDTO.getPageSize());

        //2.调用mapper
        Page<DishVO> page = dishMapper.page(dishPageQueryDTO);

        //目标：返回分页对象pageresult
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    @Transactional
    public void deleteBatch(List<Long> ids) {
        //1.判断删除的菜品有没有处于起售状态的，如果有就不能删除
        for (Long id : ids) {
            Dish dish = dishMapper.getById(id);
            if(dish.getStatus()== StatusConstant.ENABLE){
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }
        //2.判断菜品有没有关联套餐，如果有不能删除
        // select setmeal_id from setmeal_dish where dish_id in (1,2,3)
        List<Long> setmealIds =  setmealDishMapper.getSetmealIdsByDishId(ids);
        if(setmealIds!=null && setmealIds.size()>0){
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }

        for (Long id : ids) {
            //3.删除菜品
            dishMapper.deleteById(id);
            //4.删除口味
            //delete from dish_flovor where dish_id=#{id}
            dishFlovorsMapper.deleteByDishId(id);
        }
    }

    @Override
    public DishVO getById(Long id) {
        //1.获取菜品信息
        Dish dish = dishMapper.getById(id);

        //2.获取口味信息
        List<DishFlavor> dishFlavors = dishFlovorsMapper.getByDishId(id);

        //3.封装数据到VO对象中
        DishVO dishVO = new DishVO();
        BeanUtils.copyProperties(dish,dishVO);
        dishVO.setFlavors(dishFlavors);

        return dishVO;
    }

    @Override
    @Transactional
    public void updateById(DishDTO dishDTO) {
        //1.修改菜品基本信息
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.update(dish);

        //2.删除口味信息
        //菜品id
        Long dishId = dishDTO.getId();
        dishFlovorsMapper.deleteByDishId(dishId);
        //3.插入新的口味信息
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if(flavors!=null && flavors.size()>0){
            //给所有的口味添加菜品id
            flavors.stream().forEach(dishFlovor->{
                dishFlovor.setDishId(dishId);
            });
            dishFlovorsMapper.insertBatch(flavors);
        }
    }

    //菜品的起售、停售
    @Override
    public void startOrStop(Integer status, Long id) {
        //菜品停售时，把套餐也停售了
        if(status==StatusConstant.DISABLE){
            List<Long> ids = new ArrayList<>();
            ids.add(id);
            //获取到菜品所在套餐的id
            List<Long> setmealIds = setmealDishMapper.getSetmealIdsByDishId(ids);

            for (Long setmealId : setmealIds) {
                Setmeal setmeal = Setmeal.builder()
                        .id(setmealId)
                        .status(StatusConstant.DISABLE)
                        .build();

                setmealMapper.update(setmeal);
            }
        }

        Dish dish = Dish.builder()
                .id(id)
                .status(status)
                .build();

        dishMapper.update(dish);
    }

    /**
     * 根据分类id查询菜品
     * @param categoryId
     * @return
     */
    public List<Dish> list(Long categoryId) {
        Dish dish = Dish.builder()
                .categoryId(categoryId)
                .status(StatusConstant.ENABLE)
                .build();

        return dishMapper.list(dish);
    }
}
